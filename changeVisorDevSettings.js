const dataBase = require('./dataBase.js');

let settings = require('./settings_millennium_smalltalk.json')

async function run(){
    await dataBase.db_dev.ref(`/clients/millennium_smalltalk/settings`).set(settings);
    console.log('done')
}

run();
