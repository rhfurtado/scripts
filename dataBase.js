const firebase = require("firebase-admin");



var serviceAccount_prd = require('./config/visor-d4538-firebase-adminsdk-1uc9m-d0cc116626.json');
var serviceAccount_dev = require('./config/visor-developers.json');


let db_prd = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount_prd),
  databaseURL: "https://visor-d4538.firebaseio.com/"
}, 'visor');

let db_dev = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount_dev),
  databaseURL: 'https://visor-developers.firebaseio.com/'
}, 'visor-dev');


console.log("Load database---VISOR");

db_dev = db_dev.database();
db_prd = db_prd.database();

// async function run(){
//   let val = await db_dev.ref(`clients/combot/settings`).once("value");
//   console.log(val.val())
// }

// run()

module.exports.db_dev = db_dev;
module.exports.db_prd = db_prd;