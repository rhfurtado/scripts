const firebase = require("firebase-admin");

// Fetch the service account key JSON file contents
const serviceAccount_aidev = require("./config/ai-dev-d890d-firebase-adminsdk-v5vu2-d0e288ac07.json");

// Initialize the app with a service account, granting admin privileges

const db_app = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount_aidev),
  databaseURL: "https://ai-dev-d890d.firebaseio.com"
}, 'ai-dev');

// As an admin, the app has access to read and write all data, regardless of Security Rules / Has admin privileges
const db_aidev = db_app.database();

const json = require("./intent.json");
const entity1 = require("./modo_envio.json");
const entity2 = require("./numero_apolice.json");

async function addEntities2BD(entity){
    let ref = db_aidev.ref('/clients/visor4/entities');
    let pushkey=ref.push().key 
    entity.key=pushkey
    await db_aidev.ref(`/clients/visor4/entities/${pushkey}`).set(entity)
    await db_aidev.ref(`/clients/visor4/entities-sandbox/${pushkey}`).set(entity)
    console.log('donne')
}

async function run(){
    //let ref = db_aidev.ref(`/clients/visor4/1600429736kyCr55I6/faq`);
    json.assistants={assistants:"jjboçe@visor.ai",date:"2020-11-25T13:06:53"} // DUMMY
    //json.key = ref.push().key
    json.key='-MN345-3RW9G8Ai_LTbq'
    
    console.log(json)
    //await db_aidev.ref(`/clients/visor4/1600429736kyCr55I6/faq/${json.key}`).set(json);
    //await db_aidev.ref(`/clients/visor4/1600429736kyCr55I6/faq-sandbox/${json.key}`).set(json);
    await db_aidev.ref(`/clients/visor4/1600429736kyCr55I6/faq/${json.key}`).set(json);
    await db_aidev.ref(`/clients/visor4/1600429736kyCr55I6/faq-sandbox/${json.key}`).set(json);
    console.log('donee')
}
run()
//addEntities2BD(entity1)
//addEntities2BD(entity2)