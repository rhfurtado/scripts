const firebase = require("firebase-admin");
const fs = require("fs");

// Fetch the service account key JSON file contents
serviceAccount = require('./config/visor-d4538-firebase-adminsdk-1uc9m-d0cc116626.json');
databaseURL = "https://visor-d4538.firebaseio.com/";
dbName = "VISOR";


// As an admin, the app has access to read and write all data, regardless of Security Rules / Has admin privileges
firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: `${databaseURL}`
});

var db = firebase.database();
module.exports.db = db;
console.log(`Load database---${dbName}`);

async function getProjectName(db, clientName, project) {
    projectName = await db.ref(`clients/${clientName}/flows/${project}/settings/name`).once('value');
    console.log(projectName.val())
    return projectName.val();
}

var clientName = 'tranquilidade';
var everyFaq = {};
db.ref(`clients/${clientName}/faq-sandbox/`).once("value", pushkey => {

    var faqObj = pushkey.val();

    db.ref(`clients/${clientName}/ml_metrics/topFaq`).orderByKey().startAt("20200601").endAt("20200624").once("value", async pushkeyTopFaq => {
        // from that day forward all faqs called object
        var topFaqObj = pushkeyTopFaq.val();

        for (var faqPushkeyIndex in topFaqObj) {
            var faqPushkey = topFaqObj[faqPushkeyIndex];

            for (var faqkeyIndex in faqPushkey) {
                var faqInfo = faqPushkey[faqkeyIndex];
                var questionInfo = faqInfo.questions

                for (var questionPushKey in questionInfo) {
                    //create the project for each question
                    var project = questionInfo[questionPushKey].project || 'sem nome';

                    if (everyFaq[project] === undefined) {
                        console.log('new project');
                        everyFaq[project] = {};
                    }

                    if (everyFaq[project][faqkeyIndex] === undefined)
                        everyFaq[project][faqkeyIndex] = faqInfo;

                    if (faqObj[faqkeyIndex] === undefined)
                        //faq key does not exist in the faqs node, it has been deleted
                        everyFaq[project][faqkeyIndex].desc = "foi apagada";

                    if (everyFaq[project][faqkeyIndex].count === undefined)
                        everyFaq[project][faqkeyIndex].count = 0; //initializes count

                    everyFaq[project][faqkeyIndex].count += 1;
                }
            }
        }

        console.log("done creating object");
        //traverse through the object to save in a csv file
        var output = "Titulo,contagem,flow,descrição\n";
        for (var projectKey in everyFaq) {
            var projectName = await getProjectName(db, clientName, projectKey);
            let faqObj = everyFaq[projectKey];
            for (var pushkey in faqObj) {
                let object = faqObj[pushkey];
                let title = object.pergunta || object.name;
                let count = Math.round((object.count / 2)) || 0;
                let description = object.desc || " ";

                output += `"${title}","${count}","${projectName}","${description}"\n`;
            }
        }
        fs.writeFileSync(`./contagemFaqs_${clientName}.csv`, output, 'utf8');
    })
});