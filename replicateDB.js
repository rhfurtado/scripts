/* 
Copies BD from Web to AI. Choose the BD accordingly
*/

const firebase = require("firebase-admin");
const _ = require("underscore");
const dataBase = require('./dataBase.js');

// Fetch the service account key JSON file contents
const serviceAccount_aidev = require("./config/ai-dev-d890d-firebase-adminsdk-v5vu2-d0e288ac07.json");
const serviceAccount_visorai = require('./config/visor-ai-firebase-adminsdk-uzr5z-6957e2dd13.json');
const train = require('./train_all.js');

// Initialize the app with a service account, granting admin privileges
const otherAPP = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount_visorai),
  databaseURL: "https://visor-ai.firebaseio.com/"
}, "visor_ai");

const db_app = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount_aidev),
  databaseURL: "https://ai-dev-d890d.firebaseio.com"
}, 'ai-dev');


// As an admin, the app has access to read and write all data, regardless of Security Rules / Has admin privileges
const db_aidev = db_app.database();
//const db_visorai = otherAPP.database();

const clients = [
  //"activobank"
  // "aitestes",	//
  // "atlanticoeuropa", //
  // "atlanticoeuropateste", // 
  // "babybel", //
  //"bai"
  // "bancobest", //
  // "bancoctt", //
  // "bni", //
  // "caseguros", //
  // "caseguros2", //
  //"cetelem"
  // "cml", //
  // "cmltestes", //
  //"combot", 
  // "compal", //
  // "credibom", //
  // "fidelidadeteste", //
  // "fidelidade" //
  // "galp", //
  // "greetings", //
  // "greetingsen", //
  // "greetingses", //
  // "heineken", //
  // "intermarche", //no work mas ja n sao clientes
  // "jamhub-test", //
  // "jeronimomartins", //
  // "knorr", //
  // "logo", //
  //"mbway", //
  //"millennium"
  //"emailbot_demo" //
  // "millenniumbcp", //
  // "mljourney", //
  // "nos", //
  // "novobancodch",
  // "oney", //
  // "onspot", //
  //"pingodoce"
  // "pg", //
  // "portalviva", //
  "portinsurance"
  // "prio",  //
  // "puzzle", //
  // "sagres", //
  // "scbraga", //
  // "sensodyne", //
  // "sonae", //erro
  // "template_banking", //
  // "template_insurance", //
  //"template_insurance_v2"//,
  // "template_telco", //
  // "templatehr", //
  // "tranquilidade", // 
  // "tranquilidade_qualidade", //
  // "tranquilidade_emails", //
  //"tranquilidademediadores"
  //"tranquilidadenegocio"
  //"tranquilidadenegocio",
  // "visor-site", //
  // "visor_rafa",
  // "visor3", //
  // "visor4", //
  // "visor5", //
  // "visordaniela", // gives error for some reason
  //"visordemo", //
  // "visorqa" //
  //"zaask"
  //"zurich"
];


const environment = ["faq-sandbox", 'faq'];
//const environment = ["faq"];
//const environment = ["faq-sandbox"];

function setFaqs(clientName, project, environment, faqs) {
  //var ref = db_visorai.ref(`/clients/${clientName}/${project}/${environment}`);
  var ref = db_aidev.ref(`/clients/${clientName}/${project}/${environment}`);
  ref.set(faqs);
}


async function getFaqs(clientName, faqpath) {
  //var faqRef = dataBase.db_dev.ref(`/clients/${clientName}/${faqpath}`);
  var faqRef = dataBase.db_prd.ref(`/clients/${clientName}/${faqpath}`);

  if (faqpath === 'faq-sandbox')
    var env = 'sandbox';
  else
    var env = 'live';

  var faqValue = await faqRef.once('value');
  faqValue = faqValue.val();

  var promiseArray = [];
  var projectArr = [];
  var projectObj = {};

  outer:
  for (keys in faqValue) {
    var faqObj = faqValue[keys];

    inner:
    for (var project in faqObj.resposta) {

      if ((typeof faqObj.resposta[project] != "object") && !projectObj['no_tab']) { //marketing old faqs organization
        //marketing client with no project and no_tab is not yet created
        console.log(`no_tab detected client ${clientName} ${faqpath}`);
        project = 'no_tab';
        projectObj[project] = {};
        projectObj[project][keys] = _.clone(faqObj);

      } else if (typeof faqObj.resposta[project] != "object") {
        //marketing client with no project in answer, so put it in no_tab project
        //console.log("faqobj to no tab detected");
        project = 'no_tab';
        projectObj[project][keys] = _.clone(faqObj);


      } else if (projectObj[project] === undefined) {
        //project is just not yet created
        console.log("project not yet created");
        projectObj[project] = {};
        projectObj[project][keys] = _.clone(faqObj);
        projectObj[project][keys].resposta = faqObj.resposta[project];

      } else {
        //normal
        //console.log("normal proccedure");
        projectObj[project][keys] = _.clone(faqObj);
        projectObj[project][keys].resposta = faqObj.resposta[project] || faqObj.resposta;
      }

      projectArr.push(project);

    }

  }

  for (var project in projectObj) {
    setFaqs(clientName, project, faqpath, projectObj[project]);
  }


  projectArr = [...new Set(projectArr)];


  for (var projectIndex = 0; projectIndex < projectArr.length; projectIndex++) {
    //issue a training for each environment for each project
    var result = await train.clientRequest('train', {
      awaitControl: false,
      project: `${projectArr[projectIndex]}`,
      clientName: `${clientName}`,
      environment: `${env}`
    });

    console.log(result);
  }

  console.log(`added client ${clientName} ${faqpath} ${projectArr}`);
};




for (var clientIndex = 0; clientIndex < clients.length; clientIndex++) {
  for (var envIndex = 0; envIndex < environment.length; envIndex++) {
    getFaqs(clients[clientIndex], environment[envIndex]);
  }
}
