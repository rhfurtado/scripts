
// env variables

// ml server url
var mlServerUrl = process.env.ML_SERVER_URL || `http://ml-visor-dev.eu-west-1.elasticbeanstalk.com`;
module.exports.mlServerUrl = mlServerUrl;

// efs dir for clients files folder
var bucketDir = process.env.BUCKET_DIR || "clients-dev";
module.exports.bucketDir = bucketDir;

// access keys for S3 access
var accessKeyId = process.env.BUCKET_ACCESS_KEY;
module.exports.accessKeyId = accessKeyId;
var secretAccessKey = process.env.BUCKET_SECRET_ACCESS_KEY;
module.exports.secretAccessKey = secretAccessKey;

// mount dir for file directory
var mountDirectory = process.env.MOUNT_DIRECTORY || `${__dirname}`;
module.exports.mountDirectory = mountDirectory;

// environment selection for DB initialization
var NODE_ENV = process.env.NODE_ENV || 'production';
module.exports.NODE_ENV = NODE_ENV;
const databaseEnv = process.env.DATABASE_ENV || "visor";
module.exports.databaseEnv = databaseEnv;

// api-keys for request verification
const ml_api_key = process.env.ML_API_KEY || "AfdfDGFvSSJJrsCVBsfssfFREQ";
module.exports.ml_api_key = ml_api_key;
const els_api_key = process.env.ELS_API_KEY || "0WbWlw1nOeJsSUVcXfKptzgm0lnZgkWw";
module.exports.els_api_key = els_api_key

// elastic-search api url
const elsURL = process.env.ELS_URL || "http://els-api-visor-dev.eu-west-1.elasticbeanstalk.com";
module.exports.elsURL = elsURL