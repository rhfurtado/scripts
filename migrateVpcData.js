/* 
Copies BD from Web to AI. Choose the BD accordingly
*/

const firebase = require("firebase-admin");
//const _ = require("underscore");

// Fetch the service account key JSON file contents
//const serviceAccount_aidev = require("./config/ai-dev-d890d-firebase-adminsdk-v5vu2-d0e288ac07.json")
const serviceAccount_combot = require("./config/combot-ai-prd.json");
const serviceAccount_aidev= require('./config/visor-ai-firebase-adminsdk-uzr5z-6957e2dd13.json');
//const train = require('./train_all.js');

// Initialize the app with a service account, granting admin privileges
// const combot = firebase.initializeApp({
//   credential: firebase.credential.cert(serviceAccount_combot),
//   databaseURL: "https://combot-ai-dev.firebaseio.com/"
// }, "combot-ai-dev");

const combot = firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount_combot),
    databaseURL: "https://combot-ai-prd-d2f6b.europe-west1.firebasedatabase.app/"
  }, "combot-ai-prd");

// const aidev = firebase.initializeApp({
//   credential: firebase.credential.cert(serviceAccount_aidev),
//   databaseURL: "https://ai-dev-d890d.firebaseio.com"
// }, 'ai-dev');

const aidev = firebase.initializeApp({
      credential: firebase.credential.cert(serviceAccount_aidev),
      databaseURL: "https://visor-ai.firebaseio.com/"
    }, 'ai-prd');

// As an admin, the app has access to read and write all data, regardless of Security Rules / Has admin privileges
const db_combot = combot.database();
const db_aidev = aidev.database();

//const nodes = ['ai_trainer','1563976491qOzLBVft','collocations','synonyms','synonyms-sandbox'];
const nodes = ['ai_trainer']
//const nodes = ['1563976491qOzLBVft']
//const nodes = ['synonyms-sandbox'];
//const nodes = ['-a']

async function checkSize(node,name){
    const size = Buffer.byteLength(JSON.stringify(node.val()))/1000000
    console.log(`${name} has ${size} MB`)
    
    const path = node.ref_.path.pieces_.join('/')
    if (size>10){
        for (let val of Object.keys(node.val())){
            let deepNode = await  db_aidev.ref(`${path}/${val}`).once("value");  
            await db_combot.ref(`${path}/${val}`).set(deepNode.val())
            console.log(`DONE! migrated node ${val} from ${path.split('/').pop()}`)
        }
    }
    else{
        await db_combot.ref(`${path}`).set(node.val())
        console.log(`DONE! Migrated ${name} from ${path.split('/')[1]}\n`)
    }
}

async function migrateClient(clientName){
    try{
        for (let node of nodes){
            let clientNode = await db_aidev.ref(`/clients/${clientName}/${node}`).once("value");    
            for (let val of Object.keys(clientNode.val())){
                let deepNode = await db_aidev.ref(`/clients/${clientName}/${node}/${val}`).once("value");  
                await checkSize(deepNode,val)
            }
        }
    }catch(error){
         console.log('ERROR')
         console.log(error)
    }
}

//migrateClient('greetings')
migrateClient('combot')