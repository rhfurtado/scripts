//change db structure to accept separate flows

const firebase = require("firebase-admin");

// Fetch the service account key JSON file contents
const serviceAccount_visorai = require('./config/visor-ai-firebase-adminsdk-uzr5z-6957e2dd13.json');

// Initialize the app with a service account, granting admin privileges
const otherAPP = firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount_visorai),
    databaseURL: "https://visor-ai.firebaseio.com/"
}, "visor_ai");

// As an admin, the app has access to read and write all data, regardless of Security Rules / Has admin privileges
const db = otherAPP.database();

synonyms = 0;

db.ref(`clients/combot/synonyms-sandbox/words`).once("value", pushkeySnap => {
    
    pushkeySnap.forEach(function (faqObj) {
        synonyms = synonyms + 1;
    })


    console.log("Final countdown:", synonyms);
})

