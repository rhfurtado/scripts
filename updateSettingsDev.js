const firebase = require("firebase-admin");

// Fetch the service account key JSON file contents
const serviceAccount_aidev = require("./config/visor-developers.json");

// Initialize the app with a service account, granting admin privileges

const db_app = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount_aidev),
  databaseURL: "https://visor-developers.firebaseio.com"
}, 'visor-dev');

// As an admin, the app has access to read and write all data, regardless of Security Rules / Has admin privileges
const visor_dev = db_app.database();


async function run(){
    let ml_modules = {
        ai_modules:true,
        defaultToVisor:true,
        visor:true
    };
    let probs = {
        probabilityCategory: 0.3,
        probabilityQuestion: 0.5
    };
    await visor_dev.ref('/clients/millennium_greetings/settings/modules/ml_modules').set(ml_modules);
    await visor_dev.ref('/clients/millennium_greetings/settings/monkeyLearn').set(probs);
    console.log('done');
}

// async function addGreetings2Faqs(){
//     for (let key in greetings){
//         await db_aidev.ref(`/clients/millenium/15447282637219ixII/faq/${key}`).set(greetings[key]);
//         await db_aidev.ref(`/clients/millenium/15447282637219ixII/faq-sandbox/${key}`).set(greetings[key]);
//         await db_aidev.ref(`/clients/millenium/1603968573yzn57uiy/faq/${key}`).set(greetings[key]);
//         await db_aidev.ref(`/clients/millenium/1603968573yzn57uiy/faq-sandbox/${key}`).set(greetings[key]);
//         console.log('added ',key)
//     } 
// }
//addGreetings2Faqs()
run();