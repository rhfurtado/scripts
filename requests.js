require('colors');
var variables = require('./variables.js')
var Client = require('node-rest-client').Client;
var client = new Client();


function getDefault_args(type) {
    let api_key;

    if (type === 'els') {
        api_key = variables.els_api_key;
    } else if (type === 'ml') {
        api_key = variables.ml_api_key;
    } else {
        console.log("SOMETHING WENT REALLY WRONG IN API-KEY".red);
        throw Error('type not available for api-key');
    }

    return {
        headers: {
            "Content-Type": "application/json",
            "x-api-key": api_key
        }
    }
}



function deployAnswer(url, clientName, status, type = 'ml') {
    get_request(`${url}/${clientName}/${status}`, {}, type)
}
module.exports.deployAnswer = deployAnswer;


/**
 * 
 * @param {string} type of request
 * @param {object} info with the client's information: ClientName,
 * project and environment
 * 
 * Sends a request to the ML API
 */
async function clientRequest(type, info, api_key = 'ml') {
    var result;
    let args = {};
    switch (type) {
        case "train":
            args.data = { awaitControl: info.awaitControl }
            result = await post_request(
                `${variables.mlServerUrl}/clients/${info.clientName}/${info.project}/${info.environment}/train`,
                args, api_key
            );

            if (!result.status) {
                console.log(`TRAIN REQUEST ERROR --> ${info.clientName} | ${info.project} | ${info.environment}`.red, result.toString('utf8'))
                return { status: 500, description: "There was an unexpected error" }
            }
            return result;
        case "status":
            result = await get_request(
                `${variables.mlServerUrl}/clients/${info.clientName}/${info.project}/${info.environment}/train/status`,
                args, api_key
            );

            if (!result.status) {
                console.log(`TRAIN REQUEST ERROR --> ${info.clientName} | ${info.project} | ${info.environment}`.red, result.toString('utf8'))
                return { status: 500, description: "There was an unexpected error" }
            }
            return result;
        case 'conflicts':
            result = await get_request(
                `${variables.mlServerUrl}/clients/${info.clientName}/${info.project}/${info.environment}/conflicts`,
                args, api_key
            );

            if (!result.status) {
                console.log(`CONFLICTS REQUEST ERROR --> ${info.clientName} | ${info.project} | ${info.environment}`.red, result.toString('utf8'))
                return { status: 500, description: "There was an unexpected error" }
            }
            return result
        default:
            break;
    }
}
module.exports.clientRequest = clientRequest;



function post_request(url, args, type) {
    let default_args = getDefault_args(type);
    args.headers = default_args.headers;

    return new Promise((resolve, reject) => {
        var client = new Client();

        var req = client.post(url, args, function (data, response) {
            req.end();
            return resolve(data);
        })

        req.on('requestTimeout', function (res) {
            console.log("Request Timeout".red, url)
            return reject({ status: 408 })
        });

        req.on('responseTimeout', function (res) {
            console.log("Response Timeout".red, url);
            req.end();
            return reject({ status: 408 })
        });

        req.on('error', function (err) {
            req.end();
            console.log("Request Error".red, url, err)
            return reject({ status: 500 })
        });
    });
}
module.exports.post_request = post_request;


function get_request(url, args, type) {
    let default_args = getDefault_args(type);
    args.headers = default_args.headers;

    return new Promise((resolve, reject) => {
        var client = new Client();

        var req = client.get(url, args, function (data, response) {
            req.end();
            return resolve(data);
        })

        req.on('requestTimeout', function (res) {
            console.log("Request Timeout".red, url)
            return reject({ status: 408 })
        });

        req.on('responseTimeout', function (res) {
            console.log("Response Timeout".red, url);
            req.end();
            return reject({ status: 408 })
        });

        req.on('error', function (err) {
            req.end();
            console.log("Request Error".red, url, err)
            return reject({ status: 500 })
        });
    });
}


function delete_request(url, args, type) {
    let default_args = getDefault_args(type);
    args.headers = default_args.headers;

    return new Promise((resolve, reject) => {

        var req = client.delete(url, args, function (data, response) {
            req.end();
            return resolve(data);
        })

        req.on('requestTimeout', function (res) {
            console.log("Request Timeout".red, url)
            return reject({ status: 408 })
        });

        req.on('responseTimeout', function (res) {
            console.log("Response Timeout".red, url);
            req.end();
            return reject({ status: 408 })
        });

        req.on('error', function (err) {
            req.end();
            console.log("Request Error".red, url, err)
            return reject({ status: 500 })
        });
    });
}
module.exports.delete_request = delete_request;