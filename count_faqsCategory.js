//change db structure to accept separate flows

const firebase = require("firebase-admin");
const fs = require('fs');
// Fetch the service account key JSON file contents
const serviceAccount_visorai = require('./config/visor-ai-firebase-adminsdk-uzr5z-6957e2dd13.json');

// Initialize the app with a service account, granting admin privileges
const otherAPP = firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount_visorai),
    databaseURL: "https://visor-ai.firebaseio.com/"
}, "visor_ai");

// As an admin, the app has access to read and write all data, regardless of Security Rules / Has admin privileges
const db = otherAPP.database();

let categories = [];

db.ref(`clients/millennium/ai_trainer/aiTrainerHistory`).once("value", pushkeySnap => {
    
    fs.writeFile("millenniumAiTrainerHistory.json", JSON.stringify(pushkeySnap), function(err) {
      if (err) {
          console.log(err);
      }
    });
    pushkeySnap.forEach(function (faqObj) {
        faqObj = faqObj.val();
        categories.push(faqObj.categoria)
    })
    console.log("Number of FAQs:", categories.length)
    let result = categories.reduce(function (acc, curr) {
            if (typeof acc[curr] == 'undefined') {
              acc[curr] = 1;
            } else {
              acc[curr] += 1;
            }
          
            return acc;
          }, {});
    console.log(result)
})


// let result = categories.reduce(function (acc, curr) {
//     if (typeof acc[curr] == 'undefined') {
//       acc[curr] = 1;
//     } else {
//       acc[curr] += 1;
//     }
  
//     return acc;
//   }, {});

//   console.log(result);