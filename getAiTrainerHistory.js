//change db structure to accept separate flows

const firebase = require("firebase-admin");
const fs = require('fs');

// Fetch the service account key JSON file contents
const serviceAccount_visorai = require('./config/visor-ai-firebase-adminsdk-uzr5z-6957e2dd13.json');

// Initialize the app with a service account, granting admin privileges
const otherAPP = firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount_visorai),
    databaseURL: "https://visor-ai.firebaseio.com/"
}, "visor_ai");

// As an admin, the app has access to read and write all data, regardless of Security Rules / Has admin privileges
const db = otherAPP.database();

let aiTrainer = {};

async function run(){
    let days = await db.ref(`clients/millennium/ai_trainer/aiTrainerHistory`).once("value")
    for (let day of (Object.keys(days.val()))){
        let keys = await db.ref(`clients/millennium/ai_trainer/aiTrainerHistory/${day}`).once("value")
        console.log(day)
        for (let key of (Object.keys(keys.val()))){
            let keys = (await db.ref(`clients/millennium/ai_trainer/aiTrainerHistory/${day}/${key}`).once("value")).val()
            aiTrainer[key]=keys;
        }
    }
    fs.writeFileSync('aiTrainerMillennium.json', JSON.stringify(aiTrainer));
    console.log('DONE');
}
run()