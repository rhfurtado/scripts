/**
 * Adds the correct key to a string according to the environment
 * @param {string} environment 
 */
function getDirByEnvironment(environment) {
    switch (environment) {
        case "sandbox":
            return "-sandbox";

        case "live":
            return "";

        default:
            break;
    }
}
module.exports.getDirByEnvironment = getDirByEnvironment;
