'use strict'
const admin = require("firebase-admin");
const prompt = require('prompt')
const tools = require("./utils/tools.js")
const process = require('process');

// 3 flow for tranquilidade
// 1550256991eubwFoVR
// 1573665691PrPx5KDz
// 1574248970Eulcobr5

/*****************************Firebase server side setup ******************************/
// Fetch the service account key JSON file contents

let serviceAccount = require('./ServiceAccount/visor-ai-firebase-adminsdk-uzr5z-6957e2dd13.json');

// Initialize the app with a service account, granting admin privileges
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://visor-ai.firebaseio.com/"
});

// As an admin, the app has access to read and write all data, regardless of Security Rules / Has admin privileges
var db = admin.database();

prompt.start();

async function count_chars() {
    console.log('🙃🙃🙃🙃 This script counts how many chars you have per client 🙃🙃🙃🙃\n');
    prompt.get(['client', 'project', 'env'], async function (err, result) {

        console.log('\nCommand-line input received:');
        console.log('Client: ' + result.client);
        console.log('Project: ' + result.project);
        console.log('Environment: '+result.env);

        if(err){
            console.log('Got an error')
            console.log(err)
            process.exit();
        }

        try{
            let env = tools.getDirByEnvironment(result.env);
            let dir = `clients/${result.client}/${result.project}/faq${env}`;
            let ref = db.ref(dir);
            console.log(`\nNode is:\n ${dir}\n`)
            let snapshot = await ref.once('value');
            snapshot=snapshot.val();
            let final_text = "";
            for (let node in snapshot){
                final_text +=" "+snapshot[node]['userSays'].join(" ");
            }
            let numberOfChars = (final_text.replace(/\s/g, "")).length;
            console.log(`The number of characters present for this node is: ${numberOfChars}`);
            process.exit();

        }

        catch (error) {
            console.log('Got an error trying to access to dir');
            console.log(error);
            process.exit()
        }
    });
    
}

count_chars();