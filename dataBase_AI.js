const firebase = require("firebase-admin");


/*************************** AI DB *******************/
// Fetch the service account key JSON file contents
const serviceAccount_aidev = require("./config/ai-dev-d890d-firebase-adminsdk-v5vu2-d0e288ac07.json");
const serviceAccount_visorai = require('./config/visor-ai-firebase-adminsdk-uzr5z-6957e2dd13.json');

// Initialize the app with a service account, granting admin privileges
const otherAPP = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount_visorai),
  databaseURL: "https://visor-ai.firebaseio.com/"
}, "visor_ai");

const db_app = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount_aidev),
  databaseURL: "https://ai-dev-d890d.firebaseio.com"
}, 'ai-dev');


// As an admin, the app has access to read and write all data, regardless of Security Rules / Has admin privileges
const db_aidev = db_app.database();
const db_visorai = otherAPP.database();

module.exports.db_aidev = db_aidev;
module.exports.db_visorai = db_visorai;