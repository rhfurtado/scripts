
var Client = require('node-rest-client').Client;
var colors = require('colors');


var url = 'https://ai.visorapis.com';
//var url = 'http://ai-api-visor-qa.eu-west-1.elasticbeanstalk.com';
//var url = 'http://ai-api-visor-dev.eu-west-1.elasticbeanstalk.com';


async function clientRequest(type, info) {
    console.log(info);

    var args = {
        headers: {
            "Content-Type": "application/json",
            "x-api-key": "AfdfDGFvSSJJrsCVBsfssfFREQ"
        }
    }
    switch (type) {
        case "train":
            args.data = { awaitControl: info.awaitControl }
            var result = await request(
                `${url}/clients/${info.clientName}/${info.project}/${info.environment}/train`,
                args
            );

            if (!result.status) {
                console.log(`TRAIN REQUEST ERROR --> ${info.clientName} | ${info.project} | ${info.environment}`.red, result.toString('utf8'))
                return { status: 500, description: "There was an unexpected error" }
            }
            return result;

        default:
            break;
    }
}
module.exports.clientRequest = clientRequest;


function request(url, args) {

    return new Promise((resolve, reject) => {
        var client = new Client();

        var req = client.get(url, args, function (data, response) {
            req.end();
            return resolve(data);
        })

        req.on('requestTimeout', function (res) {
            console.log("Request Timeout".red, url)
            return reject({ status: 408 })
        });

        req.on('responseTimeout', function (res) {
            console.log("Response Timeout".red, url);
            req.end();
            return reject({ status: 408 })
        });

        req.on('error', function (err) {
            req.end();
            console.log("Request Error".red, url, err)
            return reject({ status: 500 })
        });
    });
}


