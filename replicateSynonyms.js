/* 
Copies BD from Web to AI. Choose the BD accordingly
*/


const firebase = require("firebase-admin");
const _ = require("underscore");
const dataBase = require('./dataBase.js');

// Fetch the service account key JSON file contents
const serviceAccount_aidev = require("./config/ai-dev-d890d-firebase-adminsdk-v5vu2-d0e288ac07.json");
const serviceAccount_visorai = require('./config/visor-ai-firebase-adminsdk-uzr5z-6957e2dd13.json');
const train = require('./train_all.js');
const { database } = require("firebase-admin");

// Initialize the app with a service account, granting admin privileges
const otherAPP = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount_visorai),
  databaseURL: "https://visor-ai.firebaseio.com/"
}, "visor_ai");

const db_app = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount_aidev),
  databaseURL: "https://ai-dev-d890d.firebaseio.com"
}, 'ai-dev');


// As an admin, the app has access to read and write all data, regardless of Security Rules / Has admin privileges
const db_aidev = db_app.database();
const db_visorai = otherAPP.database();

const clients = [
  // "aitestes",	//
  // "atlanticoeuropa", //
  // "atlanticoeuropateste", // 
  // "babybel", //
  //"bai"//
  // "bancobest", //
  // "bancoctt", //
  // "bni", //
  // "caseguros", //
  // "caseguros2", //
  // "cml", //
  // "cmltestes", //
  // "combot", //
  // "compal", //
  // "credibom", //
  // "fidelidadeteste", //
  // "galp", //
  // "greetings", //
  // "greetingsen", //
  // "greetingses", //
  // "heineken", //
  // "intermarche", //no work mas ja n sao clientes
  // "jamhub-test", //
  // "jeronimomartins", //
  //"fidelidade"
  // "knorr", //
  // "logo", //
  // "mbway", //
  // "millenniumbcp", //
  // "mljourney", //
  // "nos", //
  // "novobancodch",
  // "oney", //
  // "onspot", //
  //"pingodoce"
  // "pg", //
  // "portalviva", //
  "portinsurance"
  // "prio",  //
  // "puzzle", //
  // "sagres", //
  // "scbraga", //
  // "sensodyne", //
  // "sonae", //erro
  // "template_banking", //
  // "template_insurance", //
  //"template_insurance_v2",
  // "template_telco", //
  // "templatehr", //
  // "tranquilidade", // 
  // "tranquilidade_qualidade", //
  // "tranquilidade_emails", //
  // "tranquilidadenegocio",
  //"tranquilidademediadores"
  // "visor-site", //
  // "visor_rafa",
  // "visor3", //
  //"visor4", //
  // "visor5", //
  // "visordaniela", // gives error for some reason
  //"visordemo", //
  // "visorqa" //
  //"zaask"
];


//const environment = ["faq-sandbox", 'faq'];
// const environment = ["faq"];

// async function removeClients(){
//   await db_visorai.ref("/clients/zurich/1591180662KSvHElQh").remove()
//   console.log('DONE')
// }

async function copySynonyms(copyClient,finalClient,prd=false,sandbox=true){
    let db = prd ? db_visorai : db_aidev;
    let environment = sandbox? 'synonyms-sandbox' : 'synonyms';
    console.log('environment used: ',environment)
    //let synonyms =await dataBase.db_dev.ref(`/clients/${copyClient}/${environment}/words`).once("value");
    let synonyms =await dataBase.db_prd.ref(`/clients/${copyClient}/${environment}/words`).once("value");

    //let synonyms = await db.ref(`/clients/${copyClient}/${environment}`).once("value");
    //console.log(synonyms.val());
    await db.ref(`/clients/${finalClient}/${environment}/words`).set(synonyms.val());
    //await db.ref(`/clients/${finalClient}`).child(environment).set(synonyms.val());
    console.log('DONE')
}

async function copyCollocations(copyClient,finalClient,prd=false,sandbox=true){
  let db = prd ? db_visorai : db_aidev;
  let environment = sandbox? 'compoundWords-sandbox' : 'compoundWords';
  //let environment = sandbox? 'collocations-sandbox' : 'collocations';
  console.log('environment used: ',environment)
  //let collocations =await dataBase.db_dev.ref(`/clients/${copyClient}/${environment}`).once("value");
  let collocations =await dataBase.db_prd.ref(`/clients/${copyClient}/${environment}`).once("value");
  
  console.log(collocations.val());

  let environment2 = sandbox? 'sandbox' : 'live';
  await db.ref(`/clients/${finalClient}/collocations/${environment2}`).set(collocations.val());
  console.log('DONE')
}

copyCollocations('portinsurance',"portinsurance",false,false);
copySynonyms("portinsurance","portinsurance",false,false);
//copySynonyms('fidelidade','fidelidade',true,true)
//copySynonyms('zurich','zurich',false,true)
//removeClients()