/* 
Copies BD from Web to AI. Choose the BD accordingly
*/

const firebase = require("firebase-admin");
const _ = require("underscore");

// Fetch the service account key JSON file contents
const serviceAccount_aidev = require("./config/combot-ai-prd.json");

const aidev = firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount_aidev),
  databaseURL: "https://combot-ai-prd-d2f6b.europe-west1.firebasedatabase.app/"
}, 'ai_prd');

const db_aidev = aidev.database();

const json = require("./config_lang.json");


async function run(){
  console.log(json);
  let ref = db_aidev.ref('/configs').set(json)
  console.log('done');
    // let i =0;
    // while(i<5){
    //     //let ref = db_aidev.ref('/').
    //     //let push=ref.push().key;
    //     console.log(push)
    //     i++;
    // }
}
run()

//migrateClient('combot')